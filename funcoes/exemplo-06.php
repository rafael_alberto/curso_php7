<?php  

	$hierarquia = array(
		array(
			'nome_cargo' => 'CEO',
			'subordinados' => array(
				//Início diretor comercial
				array(
					'nome_cargo' => 'Diretor Comercial',
					'subordinados' => array(
						//Início gerente de vendas
						array(
							'nome_cargo' => 'Gerente de Vendas',
						),
						//Fim gerente de vendas
						//Início gerente de compras
						array(
							'nome_cargo' => 'Gerente de Compras',
						)
						//Fim gerente de compras
					)
				),
				//Término diretor comercial
				//Início diretor financeiro
				array(
					'nome_cargo' => 'Diretor Financeiro'
				)
				//Término diretor financeiro
			)
		)
	);

	function exibe($cargos){
		$html = '<ul>';

		foreach ($cargos as $cargo) {
			$html .= '<li>';
			$html .= $cargo['nome_cargo'];

			if (isset($cargo['subordinados']) && count($cargo['subordinados']) > 0) {
				$html .= exibe($cargo['subordinados']);
			}

			$html .= '</li>';
		}

		$html .= '</ul>';
		return $html;
	}

	echo exibe($hierarquia);
?>