<?php  

	require_once("config.php");

	$usuarioLogado = isset($_SESSION["nome"]) ? $_SESSION["nome"] : NULL;

	if(!is_null($usuarioLogado)){
		echo "Usuário: " . $usuarioLogado;
	}else{
		echo "Usuário não logado!";
	}

	session_unset($usuarioLogado);

?>