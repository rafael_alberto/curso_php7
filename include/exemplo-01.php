<?php  
	
	/*
		include - tenta funcionar mesmo se o arquivo estiver com problema
		require - obriga que o arquivo exista e que esteja funcionando corretamente
		include_once / require_once - evita chamar o mesmo método mais de uma vez, em caso dois ou mais arquivos importados possuírem o mesmo método
	*/

	require_once "functions/function.php";

	$resultado = somar(10, 5);

	echo $resultado;

?>