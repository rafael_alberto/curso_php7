<?php  

	class Pessoa {

		public $nome = "Rasmus Lerdorf"; //criador do PHP
		protected $idade = 49;
		private $senha = "123";

		public function verDados(){
			echo "Nome: " . $this->nome . "<br>" . 
				 "Idade: " . $this->idade . "<br>" . 
				 "Senha: " . $this->senha . "<br>";
		}

	}

	class Programador extends Pessoa {
		//atributo/método do tipo private não herda da classe Pai
		
		public function verDados(){
			
			echo "Class: " . get_class($this) . "<br>";

			echo "Nome: " . $this->nome . "<br>" . 
				 "Idade: " . $this->idade . "<br>" . 
				 "Senha: " . $this->senha . "<br>";
		}
	}

	$programador = new Programador();
	$programador->verDados();

?>