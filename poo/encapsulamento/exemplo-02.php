<?php  

	class Pessoa {

		private $nome;
		private $documento;

		public function __construct($nome, $documento){
			$this->nome = $nome;
			$this->documento = $documento;
		}

		public function getNome(){
			return $this->nome;
		}

		public function setNome($nome){
			$this->nome = $nome;
		}

		public function getDocumento(){
			return $this->documento;
		}

		public function setDocumento($documento){
			$this->documento = $documento;
		}

		public function __toString(){
			return $this->nome . ", " . $this->documento;
		}
	}

	$pessoa = new Pessoa("Rafael", "36512073827");
	echo "Nome: " . $pessoa->getNome() . " - Documento: ". $pessoa->getDocumento();
	echo "<br>";
	echo $pessoa;
?>