<?php  

	class Pessoa {

		public $nome;

		public function exibir(){
			return "O meu nome é " . $this->nome;
		}

	}

	$pessoa = new Pessoa();
	$pessoa->nome = "Rafael";

	echo $pessoa->exibir();
?>