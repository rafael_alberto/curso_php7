<?php  

	interface CRUD {

		public function inserir();
		public function atualizar($id);
		public function excluir($id);
		public function listar();

	}
	
	abstract class BancoDeDados implements CRUD {

		public function inserir(){
			echo "inserindo registro" . "<br>";
		}

		public function atualizar($id){
			echo "atualizando registro " . $id . "<br>";
		}

		public function excluir($id){
			echo "excluindo registro " . $id . "<br>";
		}

		public function listar(){
			echo "listando registros" . "<br>";
		}

	}

?>