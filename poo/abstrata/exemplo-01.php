<?php  

	define("SEPARADOR", "<br>");

	interface CRUD {

		public function inserir();
		public function atualizar($id);
		public function excluir($id);
		public function listar();

	}

	abstract class BancoDeDados implements CRUD {

		public function inserir(){
			echo "inserindo registro" . SEPARADOR;
		}

		public function atualizar($id){
			echo "atualizando registro " . $id . SEPARADOR;
		}

		public function excluir($id){
			echo "excluindo registro " . $id . SEPARADOR;
		}

		public function listar(){
			echo "listando registros" . SEPARADOR;
		}

	}

	class Postgres extends BancoDeDados {

		public function getNome(){
			echo "Postgres" . SEPARADOR;
		}

	}

	$bancoDeDados = new Postgres();
	$bancoDeDados->inserir();
	$bancoDeDados->atualizar("10");
	$bancoDeDados->excluir("2");
	$bancoDeDados->listar();
	$bancoDeDados->getNome();
?>