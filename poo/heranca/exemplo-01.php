<?php  

	class Documento {

		private $numero;

		public function getNumero(){
			return $this->numero;
		}

		public function setNumero($numero){
			$this->numero = $numero;
		}

	}

	class Cpf extends Documento {

		public function validarCpf():bool{
			return true;
		}

	}

	$cpf = new Cpf();
	$cpf->setNumero("11144477735");

	echo $cpf->getNumero();
	echo "<br>";
	var_dump($cpf->validarCpf());
?>