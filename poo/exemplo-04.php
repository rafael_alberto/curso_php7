<?php  

	class ContaBancaria {
		
		private $titular;
		private $tipoConta;
		private $saldo;

		public function getTitular(){
			return $this->titular;
		}

		public function setTitular($titular){
			$this->titular = $titular;
		}

		public function getTipoConta(){
			return $this->tipoConta;
		}

		public function setTipoConta($tipoConta){
			$this->tipoConta = $tipoConta;
		}

		public function getSaldo():float{
			return $this->saldo;
		}

		public function setSaldo($saldo){
			$this->saldo = $saldo;
		}

		public function exibirDados(){
			return array(
				"titular" => $this->getTitular(),
				"tipoConta" => $this->getTipoConta(),
				"saldo" => $this->getSaldo()
				);
		}
	}

	$contaCorrente = new ContaBancaria();
	$contaCorrente->setTitular("Rafael Alberto");
	$contaCorrente->setTipoConta("Conta Corrente");
	$contaCorrente->setSaldo("5400.50");

	echo json_encode($contaCorrente->exibirDados());
?>