<?php  

	class Endereco {

		private $logradouro;
		private $numero;
		private $cidade;

		public function __construct($logradouro, $numero, $cidade){
			$this->logradouro = $logradouro;
			$this->numero = $numero;
			$this->cidade = $cidade;
		}

		public function __toString(){
			return $this->logradouro. ", " . $this->numero . ", " . $this->cidade;
		}

		public function exibir(){
			return array(
					"logradouro" => $this->logradouro,
					"numero" => $this->numero,
					"cidade" => $this->cidade
				);
		}
	}

	$endereco = new Endereco("Rua Madre Tereza de Calcutá", "241", "Lençóis Paulista - SP");

	echo json_encode($endereco->exibir());
	echo "<br>";
	echo "toString: " . $endereco;

?>