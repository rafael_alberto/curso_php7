<?php  

	define("SEPARADOR", "<br>");

	interface CRUD {

		public function inserir();
		public function atualizar($id);
		public function excluir($id);
		public function listar();

	}

	class BancoDeDados implements CRUD {

		public function inserir(){
			echo "inserindo registro" . SEPARADOR;
		}

		public function atualizar($id){
			echo "atualizando registro " . $id . SEPARADOR;
		}

		public function excluir($id){
			echo "excluindo registro " . $id . SEPARADOR;
		}

		public function listar(){
			echo "listando registros" . SEPARADOR;
		}

	}

	$bancoDeDados = new BancoDeDados();
	$bancoDeDados->inserir();
	$bancoDeDados->atualizar("10");
	$bancoDeDados->excluir("2");
	$bancoDeDados->listar();
?>