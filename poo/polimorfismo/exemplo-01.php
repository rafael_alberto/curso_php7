<?php  

	abstract class Animal {

		private $nome;

		public function __construct($nome){
			$this->nome = $nome;
		}

		public function getNome(){
			return $this->nome;
		}

		public function falar(){
			return "som";
		}

		public function mover(){
			return "anda";
		}
	}

	class Cachorro extends Animal {

		public function falar(){
			return "Late";
		}
	}

	class Gato extends Animal {

		public function falar(){
			return "Miar";
		}
	}

	class Passaro extends Animal {

		public function falar(){
			return "Canta";
		}

		public function mover(){
			return "Voa e " . parent::mover();
		}

	}

	$cachorro = new Cachorro("Pluto");
	echo $cachorro->getNome() . "<br>";
	echo $cachorro->falar() . "<br>";
	echo $cachorro->mover() . "<br>";

	echo "--------------------------<br>";

	$gato = new Gato("Tom");
	echo $gato->getNome() . "<br>";
	echo $gato->falar() . "<br>";
	echo $gato->mover() . "<br>";

	echo "--------------------------<br>";

	$passaro = new Passaro("Pica-pau");
	echo $passaro->getNome() . "<br>";
	echo $passaro->falar() . "<br>";
	echo $passaro->mover() . "<br>";

?>