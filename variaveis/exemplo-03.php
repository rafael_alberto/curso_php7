<?php

	$nome = "Rafael";
	$ano = 1989;
	$salario = 1000.50;
	$ativo = true;
	$idiomas = array("Português", "Inglês", "Espanhol", "Italiano");
	$dataAtual = new DateTime();

	$separador = "<br>";

	echo $nome . $separador;
	echo $ano . $separador;
	echo $salario . $separador;
	echo $ativo . $separador;

	echo $idiomas[0] . ", " . $idiomas[1] . ", " . $idiomas[2] . ", " . $idiomas[3] . $separador;
	//var_dump($dataAtual);
	echo date_format($dataAtual, 'd/m/Y');

	$arquivo = fopen("exemplo-03.php", "r");
	//var_dump($arquivo);

	$nulo = NULL; //não tem espaço na memória - varíavel destruída)
	$vazio = ""; //vazio - iniciado porém sem informação

?>