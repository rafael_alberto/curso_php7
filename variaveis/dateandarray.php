<?php

	echo "<h4>Revisão Variáveis</h4>";
	
	$notas = array (9.5, 10);

	$media = ($notas[0] + $notas[1]) / 2; 
	
	$data = new DateTime();

	$separador = "<br>";

	echo "Notas: " . $notas[0] . " - " . $notas[1] . $separador;
	
	echo "Média: " . $media . $separador;

	echo "Data: " . date_format($data, "d/m/Y");

	echo $separador;
	
?>