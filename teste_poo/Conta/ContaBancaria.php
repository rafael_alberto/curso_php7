<?php  

	namespace Conta;

	abstract class ContaBancaria {

		private $numero;
		private $titular;

		public function getNumero():string{
			return $this->numero;
		}

		public function setNumero($numero){
			$this->numero = $numero;
		}

		public function getTitular():string{
			return $this->titular;
		}

		public function setTitular($titular){
			$this->titular = $titular;
		}

	}


?>