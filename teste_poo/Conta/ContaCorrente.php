<?php  

	namespace Conta;

	class ContaCorrente extends ContaBancaria {

		private $saldo;

		public function getSaldo():string{
			return $this->saldo;
		}

		public function setSaldo($saldo){
			$this->saldo = $saldo;
		}

		public function __toString(){
			return json_encode(array(
					"numero" => $this->getNumero(),
					"titular" => $this->getTitular(),
					"saldo" => $this->getSaldo()
				));	
		}

	}

?>