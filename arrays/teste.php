<?php  

	define(SEPARADOR, "<br>");

	$paises = array();

	array_push($paises, ["nome" => "Brasil", "capital" => "Brasília"]);
	array_push($paises, ["nome" => "Estados Unidos", "capital" => "Washington"]);
	array_push($paises, ["nome" => "França", "capital" => "Paris"]);
	array_push($paises, ["nome" => "Argentina", "capital" => "Buenos Aires"]);

	print_r($paises);

	echo SEPARADOR . SEPARADOR;

	sort($paises);

	foreach ($paises as $pais) {
		echo "País: $pais[nome]" . " - Capital: $pais[capital]" . SEPARADOR;
	}

	echo SEPARADOR;

	$json = json_encode($paises);

	echo $json;

?>