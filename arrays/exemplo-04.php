<?php  

	$paises = array();

	array_push($paises, array('nome' => 'Brasil', 'capital' => 'Brasília'));
	array_push($paises, array('nome' => 'Estados Unidos', 'capital' => 'Washington'));
	array_push($paises, array('nome' => 'Canadá', 'capital' => 'Ottawa'));
	array_push($paises, array('nome' => 'França', 'capital' => 'Paris'));
	array_push($paises, array('nome' => 'Portugal', 'capital' => 'Lisboa'));

	//print_r($paises);
	echo json_encode($paises);

?>