<?php  

	define('HOST', '127.0.0.1');
	define('USER', 'root');
	define('PASSWORD', '');
	define('DATABASE', 'dbphp7');
	
	$conexao = new mysqli(HOST, USER, PASSWORD, DATABASE);
	
	if ($conexao->connect_error) {
    	echo "Erro ao conectar: " . $conexao->connect_error;
    	exit;
	}

	$resultado = $conexao->query("select * from usuarios");

	$dados = array();

	while($row = $resultado->fetch_assoc()){
		array_push($dados, $row);
	}

	echo json_encode($dados);

?>