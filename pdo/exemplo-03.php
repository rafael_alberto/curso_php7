
<?php  

	$connection = new PDO("pgsql:host=127.0.0.1;dbname=cursophp7", "postgres", "postgres");

	$statement = $connection->prepare("SELECT NEXTVAL('seq_usuarios') as id");
	$statement->execute();

	$id = $statement->fetch()['id'];

	$statement = $connection->prepare("INSERT INTO usuarios (id, usuario, senha, data_cadastro) values (:id, :usuario, :senha, :data_cadastro)");

	$usuario = "rafael";
	$senha = "123456";
	$dataCadastro = date("Y-m-d H:i:s");

	$statement->bindParam(":id", $id);
	$statement->bindParam(":usuario", $usuario);
	$statement->bindParam(":senha", $senha);
	$statement->bindParam(":data_cadastro", $dataCadastro);

	$statement->execute();
	echo "Dados cadastrados com sucesso!";

?>