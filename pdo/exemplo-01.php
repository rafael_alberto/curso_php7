<?php  

	$conexao = new PDO("mysql:dbname=dbphp7;host=127.0.0.1", "root", "");

	$statement = $conexao->prepare("SELECT * FROM usuarios");
	$statement->execute();

	$results = $statement->fetchAll(PDO::FETCH_ASSOC);

	foreach ($results as $result) {

		$dataCadastro = new DateTime($result['data_cadastro']);

		echo "ID: " . $result['id'] . 
			 " - Usuário: " . $result['usuario'] . 
			 " - Senha: " . $result['senha'] . 
			 " - Data cadastro: ". $dataCadastro->format('d/m/y H:i:s') . "<br>";
	}

	//echo json_encode($results);
?>