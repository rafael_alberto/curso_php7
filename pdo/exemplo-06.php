<?php  

	$connection = new PDO("pgsql:host=127.0.0.1;dbname=cursophp7", "postgres", "postgres");
	
	$connection->beginTransaction();

	$statement = $connection->prepare("DELETE FROM usuarios WHERE id = ?");

	$id = 5;

	$statement->execute(array($id));

	//$connection->rollback();
	$connection->commit();

	echo "Delete OK!";

?>