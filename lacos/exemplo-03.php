<?php

	$notas = array(10, 9.5, 9);
	$mediaFinal = calcularMedia($notas);

	echo "Média final: ${mediaFinal}";

	function calcularMedia($notas) {
		$quantidadeNotas = sizeof($notas);
		$totalNota = 0;
		
		/*for ($index = 0; $index < $quantidadeNotas; $index ++) { 
			$totalNota += $notas[$index];
		}*/

		foreach ($notas as $nota) {
			$totalNota += $nota;
		}

		return ($totalNota / $quantidadeNotas);
	}

?>