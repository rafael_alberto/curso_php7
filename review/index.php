<?php 

	define("SEPARADOR", "<br>");

	class Cerveja {

		private $sku;
		private $nome;

		public function __construct($sku, $nome){
			$this->sku = $sku;
			$this->nome = $nome;
		}

		public function getSku(){
			return $this->sku;
		}

		public function setSku($sku){
			$this->sku = $sku;
		}

		public function getNome(){
			return $this->nome;
		}

		public function setNome($nome){
			$this->nome = $nome;
		}

		public function __toString(){
			return $this->sku . "," . $this->nome;
		}

	}

	$cerveja = new Cerveja("R17", "Heineken");
	//$cerveja->setSku("A01");
	//$cerveja->setNome("Brahma");

	echo $cerveja->getSku();
	echo SEPARADOR;
	echo $cerveja->getNome();
	echo SEPARADOR;
	echo $cerveja;
 ?>