<!DOCTYPE html>
<html>
<head>
	<title>Testando Form com PHP</title>
</head>
<body>
	<h2>Cadastro</h2>
	<form method="post" action="form.php">
		<label>Nome: </label><input type="text" name="nome" autofocus><br>
		<label>Idade: </label><input type="text" name="idade" autofocus><br><br>
		<input type="submit">
	</form>
	<br>
	
</body>
</html>

<?php  

	$nome = isset($_POST["nome"]) ? $_POST["nome"] : NULL;
	$idade = isset($_POST["idade"]) ? $_POST["idade"] : NULL;

	define("DBNAME", "brewer");
	define("HOST", "127.0.0.1");
	define("USER", "postgres");
	define("PASSWORD", "postgres");

	$dbURL = "pgsql:dbname=" . DBNAME . ";host=" . HOST . "";

	$connection = new PDO($dbURL, USER, PASSWORD);
	$statement = $connection->prepare("SELECT * FROM cervejas");
	$statement->execute();

	$cervejas = $statement->fetchAll(PDO::FETCH_ASSOC);

	echo '<table>' .
		 	'<thead>' .
				'<tr>' .
					'<th>Nome</th>' . 
					'<th>Idade</th>' .
				'</tr>' .
		 	'</thead>' .
		 	'<tbody>';
				foreach ($cervejas as $cerveja) {
					echo '<tr>' .
							 '<td>' . $cerveja['nome'] . '<td>' .
					    	 '<td>' . $cerveja['sabor'] . '<td>' .
					     '</tr>';
				}
	echo 	'</tbody>' .
	     '</table>';
?>
