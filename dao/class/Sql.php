<?php  

	class Sql extends PDO {

		private $connection;

		public function __construct(){
			$this->connection = new PDO("pgsql:host=127.0.0.1;dbname=brewer-php", "postgres", "postgres");
		}

		public function query($rawQuery, $parameters = array()){
			$statement = $this->connection->prepare($rawQuery);
			$this->setParams($statement, $parameters);
			$statement->execute();
			return $statement;
		}

		public function select($rawQuery, $parameters = array()):array{
			$statement = $this->query($rawQuery, $parameters);
			return $statement->fetchAll(PDO::FETCH_ASSOC);
		}

		private function setParams($statement, $parameters = array()){
			foreach ($parameters as $key => $value) {
				$this->setParam($statement, $key, $value);
			}
		}

		private function setParam($statement, $key, $value){
			$statement->bindParam($key, $value);
		}

	}	

?>