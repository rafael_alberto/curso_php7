<?php  

	class Cerveja{

		private $id;
		private $sku;
		private $nome;
		private $teorAlcoolico;
		private $valor;
		private $estoque;

		public function getId(){
			return $this->id;
		}

		public function setId($id){
			$this->id = $id;
		}

		public function getSku(){
			return $this->sku;
		}

		public function setSku($sku){
			$this->sku = $sku;
		}

		public function getNome(){
			return $this->nome;
		}

		public function setNome($nome){
			$this->nome = $nome;
		}

		public function getTeorAlcoolico(){
			return $this->teorAlcoolico;
		}

		public function setTeorAlcoolico($teorAlcoolico){
			$this->teorAlcoolico = $teorAlcoolico;
		}

		public function getValor(){
			return $this->valor;
		}

		public function setValor($valor){
			$this->valor = $valor;
		}

		public function getEstoque(){
			return $this->estoque;
		}

		public function setEstoque($estoque){
			$this->estoque = $estoque;
		}

		public function findById($id){
			$sql = new Sql();
			$result = $sql->select("SELECT * FROM cervejas WHERE id = :id", array(
				":id"=>$id
			));

			if(count($result) > 0){
				$this->setData($result[0]);
			}
		}

		private function setData($row){
			$this->setId($row['id']);
			$this->setSku($row['sku']);
			$this->setNome($row['nome']);
			$this->setTeorAlcoolico($row['teor_alcoolico']);
			$this->setValor($row['valor']);
			$this->setEstoque($row['estoque']);
		}

		public static function getList(){
			$sql = new Sql();
			$results = $sql->select("SELECT * FROM cervejas ORDER BY id ASC");
			return $results;
		}

		public static function search($nome){
			$sql = new Sql();
			$result = $sql->select("SELECT * FROM cervejas WHERE nome LIKE :search ORDER BY id ASC", array(
					":search" => "%" . $nome . "%"
				));
			return $result;
		}

		public function insert(){
			$sql = new Sql();
			$results = $sql->query("SELECT nextval('seq_cervejas') as id");
			$this->setId($results->fetch()['id']);
			
			$sql->query("INSERT INTO cervejas (id, sku, nome, valor, teor_alcoolico, estoque) VALUES (:id, :sku, :nome, :valor, :teorAlcoolico, :estoque)", array(
					":id" => $this->getId(),
					":sku" => $this->getSku(),
					":nome" => $this->getNome(),
					":valor" => $this->getValor(),
					":teorAlcoolico" => $this->getTeorAlcoolico(),
					":estoque" => $this->getEstoque()
			));
		}

		public function update(){
			$sql = new Sql();
			$sql->query("UPDATE cervejas SET sku = :sku, nome = :nome, valor = :valor, teor_alcoolico = :teorAlcoolico, estoque = :estoque WHERE id = :id,", array(
					":sku" => $this->getSku(),
					":nome" => $this->getNome(),
					":valor" => $this->getValor(),
					":teorAlcoolico" => $this->getTeorAlcoolico(),
					":estoque" => $this->getEstoque(),
					":id" => $this->getId()
			));
		}

		public function delete(){
			$sql = new Sql();
			$sql->query("DELETE FROM cervejas WHERE id = :id", array(
				":id" => $this->getId()
			));
		}

		public function __toString(){
			return json_encode(array(
					"id" => $this->getId(),
					"sku" => $this->getSku(),
					"nome" => $this->getNome(),
					"teorAlcoolico" => $this->getTeorAlcoolico(),
					"valor" => $this->getValor(),
					"estoque" => $this->getEstoque()
			));
		}

	}

?>