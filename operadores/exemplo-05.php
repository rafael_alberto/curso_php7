<?php  

	$valor1 = 35;
	$valor2 = 45;

	echo "Valor 1: " . $valor1 . "<br>";
	echo "Valor 2: " . $valor2 . "<br>";

	//<=> spaceship return -1, 0 or 1
	var_dump($valor1 <=> $valor2);
	
	echo "<br>";
	echo "1 <=> 1 retorna " . (1 <=> 1) . "<br>";
	echo "1 <=> 2 retorna " . (1 <=> 2) . "<br>";
	echo "1 <=> 0 retorna " . (1 <=> 0) . "<br>";

	$retorno = $valor1 <=> $valor2;

	if($retorno == 0){
		echo "valores são iguais";	
	}elseif ($retorno == 1) {
		echo "valor 1 é maior que valor 2";
	}elseif ($retorno == -1) {
		echo "valor 1 é menor que valor 2";
	}
?>